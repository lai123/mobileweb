## Bootstrap 对设备的划分


### 超小设备手机（<768px）	小型设备平板电脑（≥768px）	中型设备台式电脑（≥992px）	大型设备台式电脑（≥1200px）


## 在bootstrap 要受bootstrap的影响最外层必须要加上 class container

## Bootstarp网格系统

Bootstrap 网格系统（Grid System）的工作原理:

行必须放置在 .container class 内，以便获得适当的对齐（alignment）和内边距（padding）。
使用行来创建列的水平组。
内容应该放置在列内，且唯有列可以是行的直接子元素。
预定义的网格类，比如 .row 和 .col-xs-4，可用于快速创建网格布局。LESS 混合类可用于更多语义布局。
列通过内边距（padding）来创建列内容之间的间隙。该内边距是通过 .rows 上的外边距（margin）取负，表示第一列和最后一列的行偏移。
网格系统是通过指定您想要横跨的十二个可用的列来创建的。例如，要创建三个相等的列，则使用三个 .col-xs-4。

Bootstrap 提供了一套响应式、移动设备优先的流式网格系统，随着屏幕或视口（viewport）尺寸的增加，系统会自动分为最多12列.

Class 前缀	col-xs-	   col-sm-	   col-md-	     col-lg-数值(1-12) 
列数量和	  12	      12	      12	       12
最小列宽	 Auto	     60px	     78px         95px





## Bootstarp 嵌套

Bootstarp 的划分是以父元素为基准,类似css布局中的百分比。

父元素中会有padding，如要清除，添加上 一层div class="row",而且加了，会自动把子元素修改为父元素的高度,会清除padding

如果嵌套，外层加上class="row" 会等分父元素的宽度,如果不加class="row"，会减去父元素的padding 30px，然后再等分.

## Bootstarp 的列偏移

关键词 offset

```
<div class="col-md-4">左边</div><div class="col-md-4 col-md-offset-4">右边</div>

```

### Bootstarp 的排序

使用关键词push 和 pull ,想象人就在元素左边，要往右移就是用push，要往左移就是用pull，命名规则

col-{设备尺寸的标识}-push-{数值 1-12}   col-{设备尺寸的标识}-pull-{数值 1-12}

```
<div class="col-md-4 col-md-push-8">左边</div><div class="col-md-4 col-md-pull-4">右边</div>

```

### 栅格系统的响应式工具

class 为 hidden-xs ,hidden-sm,hidden-md,hidden-lg 可以指定在特定的型号隐藏. 相反的 visible 为指定的设备显示

```
   <div class="container">
        <div class="row">
           <div class="col-md-4 hidden-sm">我是第一份内容</div>
           <div class="col-md-4 hidden-md">我是第二份内容</div>
           <!-- hidden-{设备尺寸} 在某个屏才隐藏
           在某个设备才显示 -->
           <div class="col-md-4 visible-xs">我是第三份内容</div>
        </div>
    </div>

```

### 作业：实现星巴克，超大屏与中等屏的样式切换（建议 bootstrap）


