## 移动端的适配

## 一 设置为320px(明显有缺陷，不推荐)

## 二 百分比（不推荐,计算相对麻烦）

## 三 rem

## 四 vm





## em(有兼容性问题)
 
简单的说就是父元素的字体大小,以父元素的font-size为准

em是相对长度单位。相对于当前对象内文本的字体尺寸。如当前对行内文本的字体尺寸未

被人为设置，则相对于浏览器的默认字体尺寸.

EM特点

1. em的值并不是固定的

2. em会继承父级元素的字体大小,但是自身设置了，以自己的为准。


## 什么是rem(root em)


px像素（Pixel）。相对长度单位。像素px是相对于显示器屏幕分辨率而言的。

PX特点

1. IE无法调整那些使用px作为单位的字体大小；
2. 国外的大部分网站能够调整的原因在于其使用了em或rem作为字体单位；
3. Firefox能够调整px和em，rem，但是96%以上的中国网民使用IE浏览器(或内核)。




## rem


rem是CSS3新增的一个相对单位（root em，根em），这个单位引起了广泛关注。这个单位与em有什么区别呢？区别在于使用rem为元素设定字体大小时，仍然是相对大小，但相对的只是HTML根元素。这个单位可谓集相对大小和绝对大小的优点于一身，通过它既可以做到只修改根元素就成比例地调整所有字体大小，又可以避免字体大小逐层复合的连锁反应。目前，除了IE8及更早版本外，所有浏览器均已支持rem。对于不支持它的浏览器，应对方法也很简单，就是多写一个绝对单位的声明。这些浏览器会忽略用rem设定的字体大小。下面就是一个例子：

## px 与 rem 的选择？
对于只需要适配少部分手机设备，且分辨率对页面影响不大的，使用px即可 。

对于需要适配各种移动设备，使用rem，例如只需要适配iPhone和iPad等分辨率差别比较挺大的设备。




## rem 移动端的布局

  可以把设备的宽度15等份（20 等分也行）,假设设计稿是375px,15 等分 一份就是25px
   
  那根元素的 font-size 25px，1rem = 25px

## JavaScript



<script>
   window.onresize=function(){
    //document对象是文档的根节点，每张网页都有自己的document对象只要浏览器开始载入 HTML 文档，该对象就存在了，可以直接使用
    console.log(document)
    console.log(document.documentElement.clientWidth)
    // 获取html的dom
    let htmlDom = document.getElementsByTagName('html')[0];
    htmlDom.style.fontSize = document.documentElement.clientWidth / 15 + 'px';
    console.log(htmlDom.style.fontSize);
   }
</script> 


## 作业:把移动端的苏宁易购中的“苏宁家电，苏宁超市....” 8个分类展示下，能达到等比的变更.







